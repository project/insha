# Insha

Insha is a simple, clean and responsive theme. The theme is not dependent on
any core theme. Its very light weight with modern look and feel.

## Table of contents

- Theme Features
- Requirements
- Installation
- Configuration
- Maintainers

## Theme Features

- Responsive, Mobile-Friendly Theme
- HTML5 and CSS3
- Mobile support (Smartphone, Tablet, Android, iPhone, etc)
- Fluid layout
- 1-column and 2-columns layout
- A total of 11 regions
- Minimal design and nice typography
- Supported standard theme features: site logo, site name, site, user pictures
in comments, user pictures in nodes.


## Requirements

This theme requires no themes outside of Drupal core.


## Installation

- Install the Insha theme as you would normally install a contributed
Drupal theme.
- Visit https://www.drupal.org/docs/extending-drupal/installing-themes for
contributed Drupal theme.


## Configuration

- Navigate to Administration > Appearance and enable the theme.


## Maintainers

- Akshay kashyap - https://www.drupal.org/u/akshay-kashyap
- Vishal Choudhary - https://www.drupal.org/u/vishal-choudhary
- Manjit Singh - [Manjit.Singh](https://www.drupal.org/u/manjitsingh-0)
- Neelam Chaudhary - [neelam.chaudhary](https://www.drupal.org/u/neelamchaudhary)
- sumit kumar - [Sumit kumar](https://www.drupal.org/u/sumit-kumar)
